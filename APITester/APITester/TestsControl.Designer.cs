﻿namespace APITester
{
    partial class TestsControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.linkLabelResultCode = new System.Windows.Forms.LinkLabel();
            this.labelResultValue = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // linkLabelResultCode
            // 
            this.linkLabelResultCode.AutoSize = true;
            this.linkLabelResultCode.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.linkLabelResultCode.Location = new System.Drawing.Point(3, 10);
            this.linkLabelResultCode.Name = "linkLabelResultCode";
            this.linkLabelResultCode.Size = new System.Drawing.Size(68, 20);
            this.linkLabelResultCode.TabIndex = 0;
            this.linkLabelResultCode.TabStop = true;
            this.linkLabelResultCode.Text = "200 OK";
            // 
            // labelResultValue
            // 
            this.labelResultValue.AutoSize = true;
            this.labelResultValue.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelResultValue.Location = new System.Drawing.Point(320, 10);
            this.labelResultValue.Name = "labelResultValue";
            this.labelResultValue.Size = new System.Drawing.Size(71, 20);
            this.labelResultValue.TabIndex = 1;
            this.labelResultValue.Text = "passing";
            // 
            // TestsControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.labelResultValue);
            this.Controls.Add(this.linkLabelResultCode);
            this.Name = "TestsControl";
            this.Size = new System.Drawing.Size(394, 41);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.LinkLabel linkLabelResultCode;
        private System.Windows.Forms.Label labelResultValue;
    }
}
