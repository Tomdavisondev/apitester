﻿namespace APITester
{
    partial class Home
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.labelLogo = new System.Windows.Forms.Label();
            this.buttonNewService = new System.Windows.Forms.Button();
            this.textBoxSearchServices = new System.Windows.Forms.MaskedTextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // labelLogo
            // 
            this.labelLogo.AutoSize = true;
            this.labelLogo.Font = new System.Drawing.Font("Candara", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelLogo.Location = new System.Drawing.Point(12, 5);
            this.labelLogo.Name = "labelLogo";
            this.labelLogo.Size = new System.Drawing.Size(148, 39);
            this.labelLogo.TabIndex = 0;
            this.labelLogo.Text = "APITester";
            // 
            // buttonNewService
            // 
            this.buttonNewService.Location = new System.Drawing.Point(358, 46);
            this.buttonNewService.Name = "buttonNewService";
            this.buttonNewService.Size = new System.Drawing.Size(100, 38);
            this.buttonNewService.TabIndex = 2;
            this.buttonNewService.Text = "New Service";
            this.buttonNewService.UseVisualStyleBackColor = true;
            this.buttonNewService.Click += new System.EventHandler(this.buttonNewService_Click);
            // 
            // textBoxSearchServices
            // 
            this.textBoxSearchServices.Location = new System.Drawing.Point(12, 56);
            this.textBoxSearchServices.Name = "textBoxSearchServices";
            this.textBoxSearchServices.Size = new System.Drawing.Size(222, 20);
            this.textBoxSearchServices.TabIndex = 3;
            this.textBoxSearchServices.TextChanged += new System.EventHandler(this.textBoxSearchServices_TextChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 40);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(35, 13);
            this.label1.TabIndex = 6;
            this.label1.Text = "label1";
            // 
            // Home
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(470, 483);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.textBoxSearchServices);
            this.Controls.Add(this.buttonNewService);
            this.Controls.Add(this.labelLogo);
            this.Name = "Home";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label labelLogo;
        private System.Windows.Forms.Button buttonNewService;
        private System.Windows.Forms.MaskedTextBox textBoxSearchServices;
        private System.Windows.Forms.Label label1;
    }
}

