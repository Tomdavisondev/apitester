﻿using MongoDB.Bson;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace APITester
{
    public partial class Home : Form
    {

        MongoClient client = new MongoDB.Driver.MongoClient(new MongoUrl("mongodb+srv://admin:admin@apitester-8dnl5.mongodb.net/test?retryWrites=true&w=majority"));

        public Home()
        {
            InitializeComponent();
            IMongoDatabase db = client.GetDatabase("Services");
            AddControls(db);
        }
        private void AddControls(IMongoDatabase db)
        {
            var coll = db.GetCollection<Service>("Services");
            var Services = coll.Find(_ => true).ToList();
            Point basePoint = new Point(12, 86);
            foreach (Service ser in Services)
            {
                ServiceControl ctrl = new ServiceControl(ser.name);
                ctrl.Location = basePoint;
                this.Controls.Add(ctrl);
                basePoint.Y += 200;
            }
        }
        private void textBoxSearchServices_TextChanged(object sender, EventArgs e)
        {
            //TODO: Need to resolve what we do when we have multiple tests present and one is not visible. Their location should move upward.
            foreach(Control gb in this.Controls)
            {
                if (gb.GetType() == typeof(ServiceControl))
                {
                    foreach (Control ctrl in gb.Controls)
                    {
                        if (ctrl.GetType() == typeof(GroupBox))
                        {
                            if (!ctrl.Text.ToLower().Contains(textBoxSearchServices.Text.ToLower()))
                            {
                                ctrl.Visible = false;
                            }
                            else
                            {
                                ctrl.Visible = true;
                            }
                        }
                    }
                }
            }
        }

        private void buttonNewService_Click(object sender, EventArgs e)
        {
            NewServiceForm ServiceForm = new NewServiceForm();
            ServiceForm.ShowDialog();
            if(ServiceForm.DialogResult == DialogResult.OK)
            {
                IMongoDatabase db = client.GetDatabase("Services");
                var coll = db.GetCollection<Service>("Services");
                coll.InsertOne(ServiceForm.ser);
                ServiceDashboard dash = new ServiceDashboard(ServiceForm.ser);
                dash.Show();
            }
        }
    }
}
