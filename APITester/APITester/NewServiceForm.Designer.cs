﻿namespace APITester
{
    partial class NewServiceForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.buttonSearchServices = new System.Windows.Forms.Button();
            this.textBoxURLEntry = new System.Windows.Forms.MaskedTextBox();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(12, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(367, 39);
            this.label1.TabIndex = 0;
            this.label1.Text = "Add a new web service";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.ForeColor = System.Drawing.SystemColors.ButtonShadow;
            this.label2.Location = new System.Drawing.Point(49, 48);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(300, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Start by entering a URL for the API or website you want to test";
            // 
            // buttonSearchServices
            // 
            this.buttonSearchServices.Location = new System.Drawing.Point(294, 74);
            this.buttonSearchServices.Name = "buttonSearchServices";
            this.buttonSearchServices.Size = new System.Drawing.Size(70, 21);
            this.buttonSearchServices.TabIndex = 6;
            this.buttonSearchServices.Text = "Import URL";
            this.buttonSearchServices.UseVisualStyleBackColor = true;
            this.buttonSearchServices.Click += new System.EventHandler(this.buttonSearchServices_Click);
            // 
            // textBoxURLEntry
            // 
            this.textBoxURLEntry.Location = new System.Drawing.Point(52, 75);
            this.textBoxURLEntry.Name = "textBoxURLEntry";
            this.textBoxURLEntry.Size = new System.Drawing.Size(242, 20);
            this.textBoxURLEntry.TabIndex = 5;
            this.textBoxURLEntry.Text = "https://example.com";
            // 
            // NewServiceForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(394, 113);
            this.Controls.Add(this.buttonSearchServices);
            this.Controls.Add(this.textBoxURLEntry);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "NewServiceForm";
            this.Text = "NewServiceForm";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button buttonSearchServices;
        private System.Windows.Forms.MaskedTextBox textBoxURLEntry;
    }
}