﻿namespace APITester
{
    partial class ServiceDashboard
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.labelService = new System.Windows.Forms.Label();
            this.tabControlServiceDash = new System.Windows.Forms.TabControl();
            this.tabPageOverview = new System.Windows.Forms.TabPage();
            this.tabPageTests = new System.Windows.Forms.TabPage();
            this.tabPageResults = new System.Windows.Forms.TabPage();
            this.tabPageMonitoring = new System.Windows.Forms.TabPage();
            this.tabPageSettings = new System.Windows.Forms.TabPage();
            this.labelOverview = new System.Windows.Forms.Label();
            this.labelResponseTime = new System.Windows.Forms.Label();
            this.labelResponseTimeValue = new System.Windows.Forms.Label();
            this.labelPassRateValue = new System.Windows.Forms.Label();
            this.labelPassRate = new System.Windows.Forms.Label();
            this.labelTotalTestsValue = new System.Windows.Forms.Label();
            this.labelTotalTests = new System.Windows.Forms.Label();
            this.labelEndpointsValue = new System.Windows.Forms.Label();
            this.labelEndpoints = new System.Windows.Forms.Label();
            this.labelTests = new System.Windows.Forms.Label();
            this.labelLatest = new System.Windows.Forms.Label();
            this.groupBoxEndpoint = new System.Windows.Forms.GroupBox();
            this.tabControlServiceDash.SuspendLayout();
            this.tabPageOverview.SuspendLayout();
            this.SuspendLayout();
            // 
            // labelService
            // 
            this.labelService.AutoSize = true;
            this.labelService.Font = new System.Drawing.Font("Candara", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelService.Location = new System.Drawing.Point(12, 9);
            this.labelService.Name = "labelService";
            this.labelService.Size = new System.Drawing.Size(116, 39);
            this.labelService.TabIndex = 1;
            this.labelService.Text = "Service";
            // 
            // tabControlServiceDash
            // 
            this.tabControlServiceDash.Controls.Add(this.tabPageOverview);
            this.tabControlServiceDash.Controls.Add(this.tabPageTests);
            this.tabControlServiceDash.Controls.Add(this.tabPageResults);
            this.tabControlServiceDash.Controls.Add(this.tabPageMonitoring);
            this.tabControlServiceDash.Controls.Add(this.tabPageSettings);
            this.tabControlServiceDash.Location = new System.Drawing.Point(19, 51);
            this.tabControlServiceDash.Name = "tabControlServiceDash";
            this.tabControlServiceDash.SelectedIndex = 0;
            this.tabControlServiceDash.Size = new System.Drawing.Size(769, 387);
            this.tabControlServiceDash.TabIndex = 2;
            // 
            // tabPageOverview
            // 
            this.tabPageOverview.Controls.Add(this.groupBoxEndpoint);
            this.tabPageOverview.Controls.Add(this.labelLatest);
            this.tabPageOverview.Controls.Add(this.labelTests);
            this.tabPageOverview.Controls.Add(this.labelEndpointsValue);
            this.tabPageOverview.Controls.Add(this.labelEndpoints);
            this.tabPageOverview.Controls.Add(this.labelTotalTestsValue);
            this.tabPageOverview.Controls.Add(this.labelTotalTests);
            this.tabPageOverview.Controls.Add(this.labelPassRateValue);
            this.tabPageOverview.Controls.Add(this.labelPassRate);
            this.tabPageOverview.Controls.Add(this.labelResponseTimeValue);
            this.tabPageOverview.Controls.Add(this.labelResponseTime);
            this.tabPageOverview.Controls.Add(this.labelOverview);
            this.tabPageOverview.Location = new System.Drawing.Point(4, 22);
            this.tabPageOverview.Name = "tabPageOverview";
            this.tabPageOverview.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageOverview.Size = new System.Drawing.Size(761, 361);
            this.tabPageOverview.TabIndex = 2;
            this.tabPageOverview.Text = "Overview";
            this.tabPageOverview.UseVisualStyleBackColor = true;
            // 
            // tabPageTests
            // 
            this.tabPageTests.Location = new System.Drawing.Point(4, 22);
            this.tabPageTests.Name = "tabPageTests";
            this.tabPageTests.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageTests.Size = new System.Drawing.Size(761, 361);
            this.tabPageTests.TabIndex = 3;
            this.tabPageTests.Text = "Tests";
            this.tabPageTests.UseVisualStyleBackColor = true;
            // 
            // tabPageResults
            // 
            this.tabPageResults.Location = new System.Drawing.Point(4, 22);
            this.tabPageResults.Name = "tabPageResults";
            this.tabPageResults.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageResults.Size = new System.Drawing.Size(761, 361);
            this.tabPageResults.TabIndex = 4;
            this.tabPageResults.Text = "Results";
            this.tabPageResults.UseVisualStyleBackColor = true;
            // 
            // tabPageMonitoring
            // 
            this.tabPageMonitoring.Location = new System.Drawing.Point(4, 22);
            this.tabPageMonitoring.Name = "tabPageMonitoring";
            this.tabPageMonitoring.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageMonitoring.Size = new System.Drawing.Size(761, 361);
            this.tabPageMonitoring.TabIndex = 5;
            this.tabPageMonitoring.Text = "Monitoring";
            this.tabPageMonitoring.UseVisualStyleBackColor = true;
            // 
            // tabPageSettings
            // 
            this.tabPageSettings.Location = new System.Drawing.Point(4, 22);
            this.tabPageSettings.Name = "tabPageSettings";
            this.tabPageSettings.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageSettings.Size = new System.Drawing.Size(761, 361);
            this.tabPageSettings.TabIndex = 6;
            this.tabPageSettings.Text = "Settings";
            this.tabPageSettings.UseVisualStyleBackColor = true;
            // 
            // labelOverview
            // 
            this.labelOverview.AutoSize = true;
            this.labelOverview.Font = new System.Drawing.Font("MS Reference Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelOverview.ForeColor = System.Drawing.Color.Gray;
            this.labelOverview.Location = new System.Drawing.Point(6, 3);
            this.labelOverview.Name = "labelOverview";
            this.labelOverview.Size = new System.Drawing.Size(164, 20);
            this.labelOverview.TabIndex = 3;
            this.labelOverview.Text = "Service Overview";
            // 
            // labelResponseTime
            // 
            this.labelResponseTime.AutoSize = true;
            this.labelResponseTime.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelResponseTime.ForeColor = System.Drawing.Color.Gray;
            this.labelResponseTime.Location = new System.Drawing.Point(6, 57);
            this.labelResponseTime.Name = "labelResponseTime";
            this.labelResponseTime.Size = new System.Drawing.Size(180, 16);
            this.labelResponseTime.TabIndex = 4;
            this.labelResponseTime.Text = "Average response time";
            // 
            // labelResponseTimeValue
            // 
            this.labelResponseTimeValue.AutoSize = true;
            this.labelResponseTimeValue.Font = new System.Drawing.Font("MS Reference Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelResponseTimeValue.ForeColor = System.Drawing.Color.Black;
            this.labelResponseTimeValue.Location = new System.Drawing.Point(47, 37);
            this.labelResponseTimeValue.Name = "labelResponseTimeValue";
            this.labelResponseTimeValue.Size = new System.Drawing.Size(75, 20);
            this.labelResponseTimeValue.TabIndex = 5;
            this.labelResponseTimeValue.Text = "186 ms";
            // 
            // labelPassRateValue
            // 
            this.labelPassRateValue.AutoSize = true;
            this.labelPassRateValue.Font = new System.Drawing.Font("MS Reference Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelPassRateValue.ForeColor = System.Drawing.Color.Black;
            this.labelPassRateValue.Location = new System.Drawing.Point(247, 37);
            this.labelPassRateValue.Name = "labelPassRateValue";
            this.labelPassRateValue.Size = new System.Drawing.Size(60, 20);
            this.labelPassRateValue.TabIndex = 8;
            this.labelPassRateValue.Text = "100%";
            // 
            // labelPassRate
            // 
            this.labelPassRate.AutoSize = true;
            this.labelPassRate.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelPassRate.ForeColor = System.Drawing.Color.Gray;
            this.labelPassRate.Location = new System.Drawing.Point(205, 57);
            this.labelPassRate.Name = "labelPassRate";
            this.labelPassRate.Size = new System.Drawing.Size(149, 16);
            this.labelPassRate.TabIndex = 7;
            this.labelPassRate.Text = "Average Pass Rate";
            // 
            // labelTotalTestsValue
            // 
            this.labelTotalTestsValue.AutoSize = true;
            this.labelTotalTestsValue.Font = new System.Drawing.Font("MS Reference Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelTotalTestsValue.ForeColor = System.Drawing.Color.Black;
            this.labelTotalTestsValue.Location = new System.Drawing.Point(69, 92);
            this.labelTotalTestsValue.Name = "labelTotalTestsValue";
            this.labelTotalTestsValue.Size = new System.Drawing.Size(20, 20);
            this.labelTotalTestsValue.TabIndex = 10;
            this.labelTotalTestsValue.Text = "1";
            // 
            // labelTotalTests
            // 
            this.labelTotalTests.AutoSize = true;
            this.labelTotalTests.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelTotalTests.ForeColor = System.Drawing.Color.Gray;
            this.labelTotalTests.Location = new System.Drawing.Point(40, 112);
            this.labelTotalTests.Name = "labelTotalTests";
            this.labelTotalTests.Size = new System.Drawing.Size(95, 16);
            this.labelTotalTests.TabIndex = 9;
            this.labelTotalTests.Text = "Total Tests";
            // 
            // labelEndpointsValue
            // 
            this.labelEndpointsValue.AutoSize = true;
            this.labelEndpointsValue.Font = new System.Drawing.Font("MS Reference Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelEndpointsValue.ForeColor = System.Drawing.Color.Black;
            this.labelEndpointsValue.Location = new System.Drawing.Point(262, 92);
            this.labelEndpointsValue.Name = "labelEndpointsValue";
            this.labelEndpointsValue.Size = new System.Drawing.Size(20, 20);
            this.labelEndpointsValue.TabIndex = 12;
            this.labelEndpointsValue.Text = "1";
            // 
            // labelEndpoints
            // 
            this.labelEndpoints.AutoSize = true;
            this.labelEndpoints.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelEndpoints.ForeColor = System.Drawing.Color.Gray;
            this.labelEndpoints.Location = new System.Drawing.Point(209, 112);
            this.labelEndpoints.Name = "labelEndpoints";
            this.labelEndpoints.Size = new System.Drawing.Size(143, 16);
            this.labelEndpoints.TabIndex = 11;
            this.labelEndpoints.Text = "Service endpoints";
            // 
            // labelTests
            // 
            this.labelTests.AutoSize = true;
            this.labelTests.Font = new System.Drawing.Font("MS Reference Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelTests.ForeColor = System.Drawing.Color.Gray;
            this.labelTests.Location = new System.Drawing.Point(6, 160);
            this.labelTests.Name = "labelTests";
            this.labelTests.Size = new System.Drawing.Size(59, 20);
            this.labelTests.TabIndex = 13;
            this.labelTests.Text = "Tests";
            // 
            // labelLatest
            // 
            this.labelLatest.AutoSize = true;
            this.labelLatest.Font = new System.Drawing.Font("MS Reference Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelLatest.ForeColor = System.Drawing.Color.Gray;
            this.labelLatest.Location = new System.Drawing.Point(353, 160);
            this.labelLatest.Name = "labelLatest";
            this.labelLatest.Size = new System.Drawing.Size(143, 20);
            this.labelLatest.TabIndex = 14;
            this.labelLatest.Text = "Latest Activity";
            // 
            // groupBoxEndpoint
            // 
            this.groupBoxEndpoint.Location = new System.Drawing.Point(10, 183);
            this.groupBoxEndpoint.Name = "groupBoxEndpoint";
            this.groupBoxEndpoint.Size = new System.Drawing.Size(342, 172);
            this.groupBoxEndpoint.TabIndex = 15;
            this.groupBoxEndpoint.TabStop = false;
            this.groupBoxEndpoint.Text = "/endpoint";
            // 
            // ServiceDashboard
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.tabControlServiceDash);
            this.Controls.Add(this.labelService);
            this.Name = "ServiceDashboard";
            this.Text = "ServiceDashboard";
            this.tabControlServiceDash.ResumeLayout(false);
            this.tabPageOverview.ResumeLayout(false);
            this.tabPageOverview.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label labelService;
        private System.Windows.Forms.TabControl tabControlServiceDash;
        private System.Windows.Forms.TabPage tabPageOverview;
        private System.Windows.Forms.Label labelLatest;
        private System.Windows.Forms.Label labelTests;
        private System.Windows.Forms.Label labelEndpointsValue;
        private System.Windows.Forms.Label labelEndpoints;
        private System.Windows.Forms.Label labelTotalTestsValue;
        private System.Windows.Forms.Label labelTotalTests;
        private System.Windows.Forms.Label labelPassRateValue;
        private System.Windows.Forms.Label labelPassRate;
        private System.Windows.Forms.Label labelResponseTimeValue;
        private System.Windows.Forms.Label labelResponseTime;
        private System.Windows.Forms.Label labelOverview;
        private System.Windows.Forms.TabPage tabPageTests;
        private System.Windows.Forms.TabPage tabPageResults;
        private System.Windows.Forms.TabPage tabPageMonitoring;
        private System.Windows.Forms.TabPage tabPageSettings;
        private System.Windows.Forms.GroupBox groupBoxEndpoint;
    }
}