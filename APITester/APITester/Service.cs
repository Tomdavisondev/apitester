﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MongoDB.Bson;

namespace APITester
{
    public class Service
    {
        public ObjectId Id { get; set; }
        public String name { get; set; }
        public List<TestResult> testResults;

        public Service(string name, List<TestResult> testResults)
        {
            this.name = name;
            this.testResults = testResults;
        }
    }
}
