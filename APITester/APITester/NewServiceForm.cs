﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace APITester
{
    public partial class NewServiceForm : Form
    {
        public Service ser;
        public string serviceId;
        public NewServiceForm()
        {
            InitializeComponent();
        }

        private void buttonSearchServices_Click(object sender, EventArgs e)
        {
            Regex rgx = new Regex(@"^http(s)?://([\w-]+.)+[\w-]+(/[\w- ./?%&=])?$");
            if (rgx.IsMatch(textBoxURLEntry.Text))
            {
                ser = new Service(textBoxURLEntry.Text);
                DialogResult = DialogResult.OK;
                this.Close();
            }
            else
            {
                DialogResult = DialogResult.Retry;
                MessageBox.Show("Entered data is not a URL");
            }
        }
    }
}
