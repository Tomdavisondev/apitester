The idea for this project is initially a basic end point tester

Users will be able to create projects, from which certain endpoints from certain domains can be tested
	This data should be queried using something like JQ if the user requires, that way they can confirm that the correct data is being returned
The idea is that once a new release is added this software can be an ongoing solution to automating API endpoint testing
The reason for this being a purely C# windows form application is so that any api can be tested, whether it's locally or on the web

Extensions:
	Adding connectivity Jenkins, everytime a build is commenced, the endpoint software will kick off another test
	Statistics allow the QA tester to identify "problem cases" within test elements. Identify changes and narrow down problems before the occur
	Unsure if this is possible: Using Chrome, Explorer, Firefox as the driver for the web request